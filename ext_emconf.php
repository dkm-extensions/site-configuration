<?php

/* * *************************************************************
 * Extension Manager/Repository config file for ext "cdsrc_bepwreset".
 *
 * Auto generated 01-02-2015 10:24
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 * ************************************************************* */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Facebook Post',
    'description' => 'Post content from News / News events to a facebook page',
    'category' => 'Backend',
    'version' => '1.0.0',
    'state' => 'alpha',
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dkm.dk',
    'author_company' => 'Danmarks Kirkelige Mediecenter',
];

