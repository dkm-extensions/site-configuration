<?php
namespace DKM\SiteConfiguration\ViewHelpers;


use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class GetSiteSettingViewHelper extends AbstractViewHelper
{
    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments()
    {
        $this->registerArgument('path', 'string', 'constant path', true);
    }


    /**
     * Renders the website title
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    public function render()
    {
        $site = self::findRootSite();
        try {
            return ArrayUtility::getValueByPath($site->getSettings()->getAll(), $this->arguments['path'], '.');
        } catch (MissingArrayPathException $e) {
            return false;
        }
    }
    static public function findRootSite(): ?Site
    {
        foreach ($GLOBALS['BE_USER']->returnWebMounts() as $webMount) {
            try {
                return GeneralUtility::makeInstance(SiteFinder::class)->getSiteByRootPageId((int)$webMount);
            } catch (SiteNotFoundException $e) {
                continue;
            }
        }
        throw new Exception('Could not find site with root');
    }

}