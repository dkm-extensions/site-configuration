<?php

namespace DKM\SiteConfiguration;

use TYPO3\CMS\Core\Utility\ArrayUtility;

class SiteConfiguration extends \TYPO3\CMS\Core\Configuration\SiteConfiguration
{
    /**
     * @throws \TYPO3\CMS\Core\Configuration\Exception\SiteConfigurationWriteException
     */
    public function setFeature(string $siteIdentifier, string $featureName, bool $activate): void
    {
        $settings = $this->getSiteSettings($siteIdentifier, [])->getAll();
        $settings = ArrayUtility::setValueByPath($settings, "features.{$featureName}.enable", ($activate ? 1 : 0), '.');
        $this->writeSettings($siteIdentifier, $settings);
    }

    public function addToSiteSettings(string $siteIdentifier, array $configuration)
    {
        $settings = $this->getSiteSettings($siteIdentifier, [])->getAll();
        ArrayUtility::mergeRecursiveWithOverrule($settings, $configuration, true, true, false);
        $this->writeSettings($siteIdentifier, $settings);
    }

    /**
     * @param int|null $rootPageId
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function isFeatureEnabled(string $featureName, int $rootPageId = null): bool
    {
        if($rootPageId) {
            $siteIdentifier = Utility::getSiteIdentifier($rootPageId);
            $settings = $this->getSiteSettings($siteIdentifier, [])->getAll();
        } else {
            $settings = $GLOBALS['TYPO3_REQUEST']->getAttribute('site')->getSettings();
        }
        return (bool)($settings['features'][$featureName]['enable'] ?? false);
    }
}