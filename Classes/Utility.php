<?php

namespace DKM\SiteConfiguration;

use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Utility
{
    /**
     * @param $rootPageId
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    static public function getSiteIdentifier($rootPageId): string
    {
        return GeneralUtility::makeInstance(SiteFinder::class)->getSiteByRootPageId($rootPageId)->getIdentifier();
    }
}